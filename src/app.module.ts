import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ScheduleModule } from '@nestjs/schedule';
import { ApolloError } from 'apollo-server-express';
import * as dotenv from 'dotenv';
import { GraphQLError, GraphQLFormattedError } from 'graphql';
import * as Redis from 'ioredis';
import { join } from 'path';
import { ArgumentValidationError } from 'type-graphql';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PROD } from './constants';
import { AuthModule } from './modules/auth/auth.module';
import { PostModule } from './modules/post/post.module';
import { UserModule } from './modules/user/user.module';
import { UserService } from './modules/user/user.service';
import { VoteModule } from './modules/vote/vote.module';
import { VoteService } from './modules/vote/vote.service';
import { createUserDataloader } from './utils/createUserDataloader';
import { createVoteDataloader } from './utils/createVoteDataloader';
import { CommentModule } from './modules/comment/comment.module';
import { createPostDataloader } from './utils/createPostDataloader';
import { PostService } from './modules/post/post.service';

dotenv.config();
const REDIS_URL = process.env.REDISCLOUD_URL;
export const redis = new Redis(
  REDIS_URL ? undefined : 6379,
  REDIS_URL ? REDIS_URL : 'localhost',
); //disgusting

@Module({
  imports: [
    MikroOrmModule.forRoot({
      dbName: 'bluedit',
      type: 'mongo',
      clientUrl: process.env.MONGO_URL,
      debug: !PROD,
      // migrations: {
      //   path: './migrations', // path to the folder with migrations
      //   pattern: /^[\w-]+\d+\.[tj]s$/,
      // },
      autoLoadEntities: true,
      ensureIndexes: true,
    }),
    GraphQLModule.forRootAsync({
      imports: [UserModule, VoteModule, PostModule],
      inject: [UserService, VoteService, PostService],
      useFactory: (
        userService: UserService,
        voteService: VoteService,
        postService: PostService,
      ) => ({
        debug: !PROD,
        autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
        formatError: (error: GraphQLError): GraphQLFormattedError => {
          if (error.originalError instanceof ApolloError) {
            return error;
          }

          if (error.originalError instanceof ArgumentValidationError) {
            const { extensions, locations, message, path } = error;

            error.extensions.code = 'GRAPHQL_VALIDATION_FAILED';

            return {
              extensions,
              locations,
              message,
              path,
            };
          }

          return error;
        },
        cors: {
          origin: 'http://localhost:4000',
          credentials: true,
        },
        context: ({ req, res }) => ({
          req,
          res,
          redis,
          userLoader: createUserDataloader(userService),
          voteLoader: createVoteDataloader(voteService),
          postLoader: createPostDataloader(postService),
        }),
      }),
    }),
    ScheduleModule.forRoot(),
    PostModule,
    UserModule,
    AuthModule,
    VoteModule,
    CommentModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
