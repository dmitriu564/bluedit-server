import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';
import { AuthStrategies } from 'src/enums';
import { SessionContext } from 'src/types';
@Injectable()
export class LoginGuard extends AuthGuard(AuthStrategies.LOCAL) {
  async canActivate(context: ExecutionContext) {
    const req = (
      GqlExecutionContext.create(context).getContext() as SessionContext
    ).req;
    const res = (await super.canActivate(context)) as boolean;
    await super.logIn(req);
    return res;
  }
  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    const req = (ctx.getContext() as SessionContext).req;
    const { options } = ctx.getArgs(); // this might fail if users are naughty
    req.body = { ...req.body, ...options };
    return req;
  }

  handleRequest(err, user, info) {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
