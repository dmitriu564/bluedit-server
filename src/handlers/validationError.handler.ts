// import { BadRequestException } from '@nestjs/common';
import { ValidationError } from 'class-validator';
import { ArgumentValidationError } from 'type-graphql';

export const validationErrorHandler = (errors: ValidationError[]) => {
  return new ArgumentValidationError(errors);
};
