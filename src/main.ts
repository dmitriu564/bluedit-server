import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as session from 'express-session';
import { AppModule, redis } from './app.module';
import { validationErrorHandler } from './handlers/validationError.handler';
import * as dotenv from 'dotenv';
import * as connectRedis from 'connect-redis';

import { PROD } from './constants';
import * as passport from 'passport';

async function bootstrap() {
  dotenv.config(); //might be redundant but makes me sleep well
  const RedisStore = connectRedis(session);
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: validationErrorHandler,
    }),
  );
  app.enableCors({
    origin: ['http://localhost:4000', 'http://93.73.169.88:4000'],
    credentials: true,
  });
  app.use(
    session({
      store: new RedisStore({
        client: redis,
        disableTouch: true,
        url: process.env.REDISCLOUD_URL || 'http://localhost:6379',
      }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 3,
        httpOnly: true,
        secure: PROD,
        sameSite: 'lax',
      },
      name: process.env.AUTH_COOKIE,
      secret: process.env.SESSION_SECRET,
      resave: false,
      saveUninitialized: false,
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session());
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
