import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { UserModule } from '../user/user.module';
import { AuthResolver } from './auth.resolver';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.strategy';
import { SessionSerializer } from './session.serializer';

@Module({
  imports: [PassportModule, UserModule],
  providers: [AuthService, SessionSerializer, AuthResolver, LocalStrategy],
  exports: [PassportModule],
})
export class AuthModule {}
