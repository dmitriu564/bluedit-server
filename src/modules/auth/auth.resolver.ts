import { UnauthorizedException, UseGuards } from '@nestjs/common';
import {
  Args,
  Context,
  Mutation,
  Query,
  ResolveField,
  Resolver,
  Root,
} from '@nestjs/graphql';
import { FORGOT_PASS_PREFIX } from 'src/constants';
import { DoesUserExist } from 'src/guards/doesUserExist.guard';
import { LoginGuard } from 'src/guards/login.guard';
import { SessionContext } from 'src/types';
import { sendEmail } from 'src/utils/sendEmail';
import { v4 } from 'uuid';
import {
  CachedUser,
  UserChangePassDTO,
  UserLoginDTO,
  UserRegDTO,
} from '../user/dto/user.dto';
import { User } from '../user/entities/user.entity';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';

@Resolver(User)
export class AuthResolver {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  //TODO: move it to user resolver
  @ResolveField(() => String)
  email(@Root() user: User, @Context() { req }: SessionContext) {
    if ((req.user as CachedUser)._id === user._id.toHexString()) {
      return user.email;
    } else {
      return '';
    }
  }

  @Mutation(() => User)
  @UseGuards(DoesUserExist)
  async register(
    @Args('options') options: UserRegDTO,
    @Context() { req }: SessionContext,
  ): Promise<User> {
    const user = await this.authService.create(options);
    // req.login(user, (done) => {
    //   console.log('reg done');
    // });
    //ctx.req.session.user = user;
    return user;
  }

  @Mutation(() => User)
  @UseGuards(LoginGuard)
  async login(
    @Args('options') options: UserLoginDTO,
    @Context() ctx: SessionContext,
  ): Promise<User> {
    return ctx.req.user as User;
  }

  @Mutation(() => Boolean)
  logout(@Context() { req, res }: SessionContext) {
    return new Promise((resolve) =>
      req.session.destroy((err) => {
        res.clearCookie(process.env.AUTH_COOKIE);
        if (err) {
          console.log(err);
          resolve(false);
          return;
        }
        resolve(true);
      }),
    );
  }

  //TODO: move it to user resolver
  @Query(() => User, { nullable: true })
  async me(@Context() { req }: SessionContext) {
    const usr = req.user as User;
    if (!usr) {
      return null;
    }
    const user = await this.userService.findOneById(usr._id);
    return user;
  }

  @Mutation(() => Boolean)
  async forgotPassword(
    @Args('email') email: string,
    @Context() { redis }: SessionContext,
  ) {
    const user = await this.userService.findOneByEmail(email);
    if (!user) {
      return true; //because we don't want people to fish emails
    }

    const token = v4();

    await redis.set(
      FORGOT_PASS_PREFIX + token,
      String(user._id),
      'ex',
      1000 * 60 * 30,
    );

    sendEmail(
      email,
      'Reset password',
      `<a href="http://localhost:4000/change-password/${token}">Change password</a>`,
    );
    return true;
  }

  @Mutation(() => User, { nullable: true })
  async changePassword(
    @Args('options') options: UserChangePassDTO,
    @Context() { redis, req }: SessionContext,
  ): Promise<User | UnauthorizedException> {
    //TODO: move it to a better place, like authService
    const key = FORGOT_PASS_PREFIX + options.token;
    const userId = await redis.get(key);
    if (!userId) {
      return new UnauthorizedException('token expired');
    }
    const user = await this.authService.updatePassword(
      userId,
      options.newPassword,
    );
    if (!user) {
      return new UnauthorizedException('User no longer exists');
    }

    /*req.login(user, (err) => {
      console.log('changePass login error: ', err);
    });*/ //find a proper way to login a user manually with passport
    redis.del(key);
    // req.login(user, (done) => {
    //   console.log('changePass done');
    // });
    return user;
  }
}
