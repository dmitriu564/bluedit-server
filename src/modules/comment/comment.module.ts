import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { Post } from '../post/entities/post.entity';
import { User } from '../user/entities/user.entity';
import { CommentResolver } from './comment.resolver';
import { CommentService } from './comment.service';
import { Comment } from './entities/comment.entity';

@Module({
  imports: [MikroOrmModule.forFeature([Comment, Post, User])],
  providers: [CommentService, CommentResolver],
})
export class CommentModule {}
