import { UseFilters, UseGuards, UseInterceptors } from '@nestjs/common';
import {
  Args,
  Context,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { HttpExceptionFilter } from 'src/filters/unauthorized.filter';
import { AuthenticatedGuard } from 'src/guards/authenticated.guard';
import { SessionContext } from 'src/types';
import { TimeMeasureInterceptor } from 'src/utils/timeMeasure.interceptor';
import { Post } from '../post/entities/post.entity';
import { CachedUser } from '../user/dto/user.dto';
import { User } from '../user/entities/user.entity';
import { CommentService } from './comment.service';
import {
  CommentInput,
  EditCommentInput,
  PaginatedComments,
} from './dto/comment.dto';
import { Comment } from './entities/comment.entity';

@Resolver(() => Comment)
export class CommentResolver {
  constructor(private readonly commentService: CommentService) {}

  @Mutation(() => Comment)
  @UseGuards(AuthenticatedGuard)
  async createComment(
    @Args('input', { type: () => CommentInput }) { postId, text }: CommentInput,
    @Args('parent', { type: () => String, nullable: true }) parent: string,
    @Context() { req }: SessionContext,
  ) {
    const user = req.user as CachedUser;
    if (parent) {
      const newComment = await this.commentService.createChildComment(
        parent,
        user._id,
        text,
      );
      return newComment;
    }
    const newComment = await this.commentService.createCommentOnPost(
      postId,
      user._id,
      text,
    );
    return newComment;
  }

  @Query(() => PaginatedComments)
  async comments(
    @Args('postId', { type: () => String }) postId: string,
    @Args('limit', { type: () => Int, defaultValue: 10 }) limit: number,
    @Args('cursor', { type: () => String, nullable: true })
    cursor: string | null,
  ): Promise<PaginatedComments> {
    return await this.commentService.findAllCommentsOnPost(
      postId,
      limit,
      cursor,
    );
  }

  @Query(() => Comment)
  async comment(
    @Args('commentId', { type: () => String }) commentId: string,
  ): Promise<Comment> {
    return await this.commentService.findOneById(commentId);
  }

  @Mutation(() => Comment)
  @UseGuards(AuthenticatedGuard)
  @UseFilters(new HttpExceptionFilter())
  deleteComment(
    @Args('commentId', { type: () => String }) commentId: string,
    @Context() { req }: SessionContext,
  ) {
    const user = req.user as CachedUser;
    return this.commentService.removeComment(user._id, commentId);
  }

  @Mutation(() => Comment)
  @UseGuards(AuthenticatedGuard)
  @UseFilters(new HttpExceptionFilter())
  editComment(
    @Args('comment', { type: () => EditCommentInput })
    comment: EditCommentInput,
    @Context() { req }: SessionContext,
  ) {
    const user = req.user as CachedUser;
    return this.commentService.updateComment(comment, user._id);
  }

  @ResolveField('author', () => User)
  async getAuthor(
    @Parent() comment: Comment,
    @Context() { userLoader }: SessionContext,
  ) {
    return await userLoader.load(comment.author.id);
  }

  @ResolveField('post', () => Post)
  async getPost(
    @Parent() comment: Comment,
    @Context() { postLoader }: SessionContext,
  ) {
    return await postLoader.load(comment.post.id);
  }

  @ResolveField('children', () => [Comment])
  async getChildren(@Parent() comment: Comment) {
    return await this.commentService.findChildren(comment.id);
  }
}
