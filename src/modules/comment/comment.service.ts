import { QueryOrder } from '@mikro-orm/core';
import { EntityRepository } from '@mikro-orm/mongodb';
import { InjectRepository } from '@mikro-orm/nestjs';
import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { exists } from 'fs';
import { NotFoundError } from 'rxjs';
import { Post } from '../post/entities/post.entity';
import { User } from '../user/entities/user.entity';
import { EditCommentInput, PaginatedComments } from './dto/comment.dto';
import { Comment } from './entities/comment.entity';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepository: EntityRepository<Comment>,
    @InjectRepository(Post)
    private readonly postRepository: EntityRepository<Post>,
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
  ) {}

  async createCommentOnPost(
    postId: string,
    userId: string,
    commentText: string,
  ): Promise<Comment | null> {
    const [user, post] = await Promise.all([
      this.userRepository.findOne({
        id: userId,
      }),
      this.postRepository.findOne({
        id: postId,
      }),
    ]);

    if (!post) {
      throw new NotFoundException('Post not found');
    }

    const newComment = new Comment({
      author: user,
      post,
      text: commentText,
    });
    await this.commentRepository.persistAndFlush(newComment);
    return newComment;
  }

  async createChildComment(
    parentId: string,
    userId: string,
    commentText: string,
  ) {
    const [parentComment, user] = await Promise.all([
      this.commentRepository.findOne({
        id: parentId,
      }),
      this.userRepository.findOne({
        id: userId,
      }),
    ]);

    if (!parentComment) {
      throw new NotFoundException("Parent comment doesn'n exist");
    }

    const newComment = new Comment({
      author: user,
      text: commentText,
      comment: parentComment,
    });

    parentComment.children.add(newComment);

    await this.commentRepository.persistAndFlush(newComment);

    return newComment;
  }

  async findAllCommentsOnPost(
    postId: string,
    limit: number,
    cursor: string,
  ): Promise<PaginatedComments> {
    const croppedLimit = Math.min(limit, 50);
    const comments = await this.commentRepository.find(
      {
        createdAt: { $lt: cursor ? new Date(cursor) : new Date() },
        post: postId,
      },
      {
        orderBy: { createdAt: QueryOrder.DESC },
        limit: croppedLimit + 1,
      },
    );
    return {
      comments: comments.slice(0, croppedLimit),
      hasMore: comments.length === croppedLimit + 1,
    };
  }

  async findOneById(commentId: string): Promise<Comment> {
    const comment = await this.commentRepository.findOne({
      id: commentId,
    });
    if (!comment) {
      throw new NotFoundException("comment doesn't exists");
    }
    return comment;
  }

  findAuthor(authorId: string) {
    return this.userRepository.findOne({
      id: authorId,
    });
  }

  async findChildren(commentId: string) {
    console.log('sijfgsbdrdiugs');
    const comment = await this.commentRepository.findOne(
      {
        id: commentId,
      },
      {
        populate: ['children'],
        fields: ['children'],
      },
    );
    return comment.children;
  }

  async removeComment(userId: string, commentId: string) {
    const commentToDelete = await this.commentRepository.findOne({
      id: commentId,
    });
    if (!commentToDelete) {
      throw new NotFoundException('Comment not found');
    }
    if (commentToDelete.author.id !== userId) {
      throw new UnauthorizedException("Cannot remove someone else's comments");
    }
    await this.commentRepository.removeAndFlush(commentToDelete);
    return commentToDelete;
  }

  async updateComment(
    comment: EditCommentInput,
    userId: string,
  ): Promise<Comment> {
    const oldComment = await this.commentRepository.findOne({
      id: comment._id,
    });
    if (!oldComment) {
      throw new NotFoundException('Comment not found');
    }
    if (oldComment.author.id !== userId) {
      throw new UnauthorizedException("Cannot edit someone else's comment");
    }
    oldComment.text = comment.text;
    await this.commentRepository.flush();
    return oldComment;
  }
}
