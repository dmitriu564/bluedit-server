import { ObjectType, Field, InputType } from '@nestjs/graphql';
import { IsMongoId, MaxLength, MinLength } from 'class-validator';
import { Comment } from '../entities/comment.entity';

@ObjectType()
export class PaginatedComments {
  @Field(() => [Comment])
  comments: Comment[];

  @Field()
  hasMore: boolean;
}

@InputType()
export class CommentInput {
  @Field({ nullable: true })
  postId?: string;

  @Field()
  @MinLength(1)
  @MaxLength(300)
  text: string;
}

@InputType()
export class EditCommentInput {
  @IsMongoId()
  @Field({ nullable: true })
  _id!: string;

  @Field()
  @MinLength(1)
  @MaxLength(300)
  text: string;
}
