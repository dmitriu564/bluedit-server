import {
  Collection,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryKey,
  Property,
  SerializedPrimaryKey,
} from '@mikro-orm/core';
import { ObjectId } from '@mikro-orm/mongodb';
import { Field, ObjectType } from '@nestjs/graphql';
import { Post } from 'src/modules/post/entities/post.entity';
import { User } from 'src/modules/user/entities/user.entity';

//TODO: add user comments with votes
interface CommentConstructorParams {
  author: User;
  text: string;
  post?: Post;
  comment?: Comment;
}

@ObjectType()
@Entity()
export class Comment {
  @Field(() => String)
  @PrimaryKey()
  _id!: ObjectId;

  @SerializedPrimaryKey()
  id!: string;

  @Field(() => Date)
  @Property({ type: 'date' })
  createdAt? = new Date();

  @Field(() => Date)
  @Property({ type: 'date', onUpdate: () => new Date() })
  updatedAt? = new Date();

  @Field()
  @Property()
  text!: string;

  @Field(() => User)
  @ManyToOne(() => User)
  author!: User;

  @Field(() => Post)
  @ManyToOne(() => Post)
  post!: Post;

  @Field(() => Comment)
  @ManyToOne(() => Comment)
  parent?: Comment;

  //TODO: add hasChildren cached value
  @Field(() => [Comment])
  @OneToMany({
    entity: () => Comment,
    mappedBy: 'parent',
    orphanRemoval: true,
  })
  children = new Collection<Comment>(this);

  constructor({ author, text, post, comment }: CommentConstructorParams) {
    this.author = author;
    this.text = text;
    if (post) this.post = post;
    if (comment) this.parent = comment;
  }
}
