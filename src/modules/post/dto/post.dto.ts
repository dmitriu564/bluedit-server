import { Field, InputType, ObjectType } from '@nestjs/graphql';
import { MaxLength, MinLength } from 'class-validator';
import { Post } from '../entities/post.entity';

@InputType()
export class PostDto {
  // @IsMongoId()
  @Field({ nullable: true })
  _id?: string;

  @Field()
  @MinLength(3)
  @MaxLength(100)
  title!: string;

  @Field()
  @MaxLength(1024)
  text!: string;

  // @Field(() => User)
  // @IsNotEmpty()
  // author!: User;
}

@ObjectType()
export class PaginatedPosts {
  @Field(() => [Post])
  posts: Post[];
  @Field()
  hasMore: boolean;
}
