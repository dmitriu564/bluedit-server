import {
  Collection,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryKey,
  Property,
  SerializedPrimaryKey,
} from '@mikro-orm/core';
import { ObjectId } from '@mikro-orm/mongodb';
import { Field, ObjectType } from '@nestjs/graphql';
import { Comment } from 'src/modules/comment/entities/comment.entity';
import { User } from 'src/modules/user/entities/user.entity';
import { Vote } from 'src/modules/vote/entities/vote.entity';

//TODO: add user comments with votes

@ObjectType()
@Entity()
export class Post {
  @Field(() => String)
  @PrimaryKey()
  _id!: ObjectId;

  @SerializedPrimaryKey()
  id!: string;

  @Field(() => Date)
  @Property({ type: 'date' })
  createdAt? = new Date();

  @Field(() => Date)
  @Property({ type: 'date', onUpdate: () => new Date() })
  updatedAt? = new Date();

  @Field()
  @Property()
  title!: string;

  @Field()
  @Property()
  text!: string;

  @Field(() => Number, { nullable: false, defaultValue: 0 })
  @Property({
    type: 'int',
    nullable: false,
  })
  totalVotes = 0;

  @Field(() => User)
  @ManyToOne(() => User)
  author!: User;

  @OneToMany(() => Vote, (v) => v.post, {
    orphanRemoval: true,
    mappedBy: 'post',
  })
  votes = new Collection<Vote>(this);

  constructor(author: User, title: string, text: string) {
    this.author = author;
    this.title = title;
    this.text = text;
  }
}
