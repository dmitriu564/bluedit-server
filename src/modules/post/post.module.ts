import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { Post } from './entities/post.entity';
import { PostService } from './post.service';
import { PostResolver } from './post.resolver';
import { User } from '../user/entities/user.entity';
import { UserModule } from '../user/user.module';
import { Vote } from '../vote/entities/vote.entity';

@Module({
  imports: [UserModule, MikroOrmModule.forFeature([Post, User, Vote])],
  providers: [PostService, PostResolver],
  exports: [PostService],
})
export class PostModule {}
