import {
  NotFoundException,
  UnauthorizedException,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import {
  Args,
  Context,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { HttpExceptionFilter } from 'src/filters/unauthorized.filter';
import { AuthenticatedGuard } from 'src/guards/authenticated.guard';
import { SessionContext } from 'src/types';
import { CachedUser } from '../user/dto/user.dto';
import { User } from '../user/entities/user.entity';
import { VoteType } from '../vote/entities/vote.entity';
import { PaginatedPosts, PostDto } from './dto/post.dto';
import { Post } from './entities/post.entity';
import { PostService } from './post.service';

@Resolver(Post)
export class PostResolver {
  constructor(private readonly postService: PostService) {}

  @Query(() => PaginatedPosts)
  posts(
    @Args('limit', { type: () => Int, defaultValue: 10 }) limit: number,
    @Args('cursor', { type: () => String, nullable: true })
    cursor: string | null,
  ): Promise<PaginatedPosts> {
    return this.postService.getAll(limit, cursor);
  }

  @Query(() => Post, { nullable: true })
  post(@Args('id', { type: () => String }) id: string): Promise<Post | null> {
    return this.postService.getById(id);
  }

  @UseGuards(AuthenticatedGuard)
  @UseFilters(new HttpExceptionFilter())
  @Mutation(() => Post)
  async createPost(
    @Args('input', { type: () => PostDto }) input: PostDto,
    @Context() { req }: SessionContext,
  ): Promise<Post> {
    const newPost = await this.postService.create(
      input,
      (req.user as User)._id,
    );
    return newPost;
  }

  @UseGuards(AuthenticatedGuard)
  @UseFilters(new HttpExceptionFilter())
  @Mutation(() => Post)
  //@UseInterceptors(TimeMeasureInterceptor)
  async votePost(
    @Args('postId', { type: () => String }) postId: string,
    @Args('vote', { type: () => Int }) vote: VoteType,
    @Context() { req }: SessionContext,
  ): Promise<Post | UnauthorizedException | NotFoundException> {
    const user = req.user as CachedUser;
    const upvotedPost = await this.postService.votePost(user._id, postId, vote);

    return upvotedPost;
  }

  @UseFilters(new HttpExceptionFilter())
  @ResolveField('author', () => User)
  async author(
    @Parent() post: Post,
    @Context() { userLoader }: SessionContext,
  ) {
    return await userLoader.load(post.author.id);
  }

  @UseGuards(AuthenticatedGuard)
  @UseFilters(new HttpExceptionFilter())
  //@Query(() => Int)
  @ResolveField('userVote', () => Int) //probably better to allow quering the specific user votes together with posts
  async userVote(
    //@Args('postId', { type: () => String }) postId: string,
    @Parent() post: Post,
    @Context() { req, voteLoader }: SessionContext,
  ) {
    const user = req.user as CachedUser;
    if (!user) {
      return 0;
    }
    const vote = await voteLoader.load({ postId: post.id, userId: user._id });
    return vote ? vote.type : 0;
  }

  @UseGuards(AuthenticatedGuard)
  @UseFilters(new HttpExceptionFilter())
  @Mutation(() => Post)
  //@UseInterceptors(TimeMeasureInterceptor) //profiling, should be removed in production build
  async deletePost(
    @Args('postId', { type: () => String }) postId: string,
    @Context() { req }: SessionContext,
  ): Promise<Post | UnauthorizedException | NotFoundException> {
    const user = req.user as CachedUser;
    const deletedPost = await this.postService.removeByAuthor(postId, user._id);

    return deletedPost;
  }

  @UseGuards(AuthenticatedGuard)
  @UseFilters(new HttpExceptionFilter())
  @Mutation(() => Post)
  async editPost(
    @Args('postToUpdate', { type: () => PostDto }) postToUpdate: PostDto,
    @Context() { req }: SessionContext,
  ): Promise<Post | UnauthorizedException | NotFoundException> {
    const user = req.user as CachedUser;
    const updatedPost = await this.postService.update(postToUpdate, user._id);

    return updatedPost;
  }

  // @ResolveField('totalVotes', () => Number)
  // async getAuthor(@Parent() post: Post) {
  //   return await this.postService.getVotes(post._id);
  // }

  // @UseGuards(AuthenticatedGuard)
  // @Mutation(() => Post, { nullable: true })
  // updatePost(
  //   @Args('id', { type: () => String }) id: string,
  //   @Args('title', { type: () => String }) title: string,
  // ): Promise<Post | null> {
  //   return this.postService.update({ _id: id, title });
  // }

  // @UseGuards(AuthenticatedGuard)
  // @Mutation(() => Post, { nullable: true })
  // deletePost(
  //   @Args('id', { type: () => String }) id: string,
  // ): Promise<Post | null> {
  //   return this.postService.remove(id);
  // }
}
