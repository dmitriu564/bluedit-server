import { QueryOrder } from '@mikro-orm/core';
import { EntityRepository, ObjectId } from '@mikro-orm/mongodb';
import { InjectRepository } from '@mikro-orm/nestjs';
import {
  ConflictException,
  Injectable,
  Logger,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { User } from '../user/entities/user.entity';
import { Vote, VoteType } from '../vote/entities/vote.entity';
import { PaginatedPosts, PostDto } from './dto/post.dto';
import { Post } from './entities/post.entity';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post)
    private readonly postRepository: EntityRepository<Post>,
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
    @InjectRepository(Vote)
    private readonly voteRepository: EntityRepository<Vote>,
  ) {}

  private readonly logger = new Logger(PostService.name);

  async getAll(limit = 10, cursor: string | null): Promise<PaginatedPosts> {
    const croppedLimit = Math.min(limit, 50);
    const posts = await this.postRepository.find(
      cursor
        ? { createdAt: { $lt: cursor ? new Date(cursor) : new Date() } }
        : {},
      {
        orderBy: { createdAt: QueryOrder.DESC },
        limit: croppedLimit + 1,
      },
    );
    return {
      posts: posts.slice(0, croppedLimit),
      hasMore: posts.length === croppedLimit + 1,
    };
  }

  async getById(id: string): Promise<Post | null> {
    return await this.postRepository.findOne({ id });
  }

  async findManyByIds(ids: string[]): Promise<Post[]> {
    return await this.postRepository.find({
      id: {
        $in: ids,
      },
    });
  }

  async create(post: PostDto, userId: ObjectId): Promise<Post> {
    console.log(post, userId);
    const user = await this.userRepository.findOne({ _id: userId });

    const newPost = new Post(user, post.title, post.text);
    user.posts.add(newPost);

    await this.userRepository.flush();

    return newPost;
  }

  async update(post: PostDto, authorId: string): Promise<Post | null> {
    const oldPost = await this.postRepository.findOne(
      {
        id: post._id,
      },
      ['author'],
    );
    if (!oldPost) {
      throw new NotFoundException("Post doesn't exist");
    }
    if (oldPost.author.id !== authorId) {
      throw new UnauthorizedException("Can't edit someone else's post");
    }
    //TODO: partial edits
    oldPost.title = post.title;
    oldPost.text = post.text;

    this.postRepository.flush();
    return oldPost;
  }

  async removeByAuthor(id: string, authorId: string): Promise<Post | null> {
    const postToDelete = await this.postRepository.findOne(
      {
        _id: new ObjectId(id),
      },
      ['author', 'votes', 'comments'], //needs to be populated for cascade remove to work
    );
    if (!postToDelete) {
      throw new NotFoundException("Post doesn't exist");
    }
    console.log(postToDelete.author.id, authorId);
    if (!(postToDelete.author.id === authorId)) {
      throw new UnauthorizedException("Can't delete someone else's post");
    }
    await this.postRepository.remove(postToDelete).flush();
    return postToDelete;
  }

  //unused, voteDataloader provides better performance
  // async getUserVote(postId: string, userId: string): Promise<null | Vote> {
  //   const vote = await this.voteRepository.findOne({
  //     post: postId,
  //     user: userId,
  //   });
  //   return vote;
  // }

  //TODO: rewrite this more
  async votePost(
    userId: string,
    postId: string,
    type: VoteType,
  ): Promise<
    Post | null | NotFoundException | UnauthorizedException | ConflictException
  > {
    //Independant async tasks, resolving them at the same time is faster
    const [post, user] = await Promise.all([
      this.postRepository.findOne(
        {
          _id: new ObjectId(postId),
        },
        ['votes'], //not redundand, needs to be populated for cascade remove to work
      ),
      this.userRepository.findOne(
        {
          _id: new ObjectId(userId),
        },
        ['votes'], //not redundand, same reason
      ),
    ]);

    if (!post) {
      throw new NotFoundException('Post not found');
    }

    if (!user) {
      throw new UnauthorizedException('User not found, unauthorized');
    }

    const oldVote = await this.voteRepository.findOne({
      post: postId,
      user: userId,
    });

    if (type === VoteType.UNVOTE) {
      console.log('oldVote', oldVote);
      if (oldVote) {
        post.votes.remove(oldVote);
        post.totalVotes -= oldVote.type;
        //await this.voteRepository.removeAndFlush(oldVote);
      }
      await this.postRepository.persistAndFlush(post);
      return post;
    }

    const realVote = type === VoteType.UPVOTE ? 1 : -1;
    if (!oldVote) {
      try {
        const vote = new Vote(realVote, user, post);
        post.totalVotes += realVote;
        await this.voteRepository.persistAndFlush(vote);
      } catch (e) {
        return post; //95% sure it's just a double vote, don't wanna spam 409 on that
      }
    } else if (oldVote.type === realVote) {
      return post;
    } else {
      post.totalVotes += -oldVote.type + realVote;
      oldVote.type = realVote;
      await this.voteRepository.flush();
    }
    return post;
  }

  async getAuthor(authorId: string) {
    return await this.userRepository.findOne({ id: authorId });
  }

  // totalVotes is cached, recount it every day to make sure the cached value is correct
  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  async recountVotes() {
    const startTime = Date.now();
    this.logger.log(`Started recounting votes`);

    const allPosts = await this.postRepository.findAll({
      populate: ['votes'],
      fields: ['totalVotes'], //no need to select all fields
    });

    allPosts.forEach((post) => {
      let votes = 0;
      post.votes.getItems().forEach((v) => {
        votes += v.type;
      });
      post.totalVotes = votes;
    });

    await this.postRepository.flush();
    const endTime = Date.now();
    this.logger.log(
      `Finished recounting. Took ${endTime - startTime} ms to recount ${
        allPosts.length
      } posts`,
    );
  }
}
