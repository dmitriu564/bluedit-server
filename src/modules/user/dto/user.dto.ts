import { Field, InputType } from '@nestjs/graphql';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsUUID,
  MinLength,
} from 'class-validator';
import { Gender } from '../entities/user.entity';

@InputType()
export class UserRegDTO {
  @Field()
  @IsNotEmpty()
  username: string;

  @Field()
  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @Field()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @Field()
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'gender must be male, female, other or unicorn',
  })
  gender: string;
}

@InputType()
export class UserLoginDTO {
  @Field({ nullable: false })
  @IsNotEmpty()
  username!: string;

  @Field({ nullable: false })
  @IsNotEmpty()
  password!: string;
}

@InputType()
export class UserChangePassDTO {
  @Field({ nullable: false })
  @IsNotEmpty()
  @IsUUID(4)
  token!: string;

  @Field({ nullable: false })
  @MinLength(8, { message: 'New password must be at lest 8 characters long' })
  @IsNotEmpty({ message: 'You must enter new password' })
  newPassword!: string;
}

//TODO: find a better place for this or ditch this out because SerializedPrimaryKey exists
export interface CachedUser {
  _id: string;
  createdAt: string;
  updatedAt: string;
  username: string;
  email: string;
  gender: string;
}
