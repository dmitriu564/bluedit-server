import {
  Cascade,
  Collection,
  Entity,
  Enum,
  OneToMany,
  PrimaryKey,
  Property,
  SerializedPrimaryKey,
} from '@mikro-orm/core';
import { Field, ObjectType } from '@nestjs/graphql';
import { ObjectId } from '@mikro-orm/mongodb';
import { Post } from 'src/modules/post/entities/post.entity';
import { Vote } from 'src/modules/vote/entities/vote.entity';

export enum Gender {
  UNICORN = 'unicorn',
  FEMALE = 'female',
  MALE = 'male',
  OTHER = 'other',
}

@ObjectType()
@Entity()
export class User {
  @Field(() => String)
  @PrimaryKey()
  _id!: ObjectId;

  @SerializedPrimaryKey()
  id!: string;

  @Field(() => Date)
  @Property({ type: 'date' })
  createdAt = new Date();

  @Field(() => Date)
  @Property({ type: 'date', onUpdate: () => new Date() })
  updatedAt = new Date();

  @Field()
  @Property({ type: 'text', unique: true })
  username!: string;

  @Field()
  @Property({ type: 'text', unique: true })
  email!: string;

  @Field()
  @Enum(() => Gender)
  gender: string;

  @Property({ type: 'text', nullable: true })
  password?: string;

  @OneToMany(() => Post, (p) => p.author)
  posts = new Collection<Post>(this);

  @OneToMany(() => Vote, (v) => v.user, {
    orphanRemoval: true,
    mappedBy: 'user',
  })
  votes = new Collection<Vote>(this);
}
