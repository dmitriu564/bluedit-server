import { EntityRepository, ObjectId } from '@mikro-orm/mongodb';
import { InjectRepository } from '@mikro-orm/nestjs';
import { Injectable } from '@nestjs/common';
import { User } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
  ) {}

  async findOneById(_id: string | ObjectId): Promise<User | null> {
    return await this.userRepository.findOne({
      _id: _id instanceof ObjectId ? _id : new ObjectId(_id),
    });
  }

  async findManyByIds(ids: string[]): Promise<User[]> {
    const users = await this.userRepository.find({
      id: {
        $in: ids,
      },
    });
    return users;
  }

  async findOneByEmail(email: string): Promise<User | null> {
    return await this.userRepository.findOne({ email });
  }
  async findOneByUsername(username: string): Promise<User | null> {
    return await this.userRepository.findOne({ username });
  }
  async create(user: User): Promise<User | null> {
    const newUser = this.userRepository.create(user);
    await this.userRepository.persistAndFlush(newUser);
    return newUser;
  }

  async update(_id: string, newUser: User): Promise<User | null> {
    let oldUser = await this.findOneById(_id);
    if (!oldUser) {
      return null;
    }
    const { _id: _, ...idlessUser } = newUser;
    oldUser = { ...oldUser, ...idlessUser }; //id omitting update on all fields
    await this.userRepository.flush();
    return oldUser;
  }

  async updatePassword(_id: string, newPassword: string): Promise<User | null> {
    const oldUser = await this.findOneById(_id);
    if (!oldUser) {
      return null;
    }
    oldUser.password = newPassword;
    await this.userRepository.flush();
    return oldUser;
  }
}
