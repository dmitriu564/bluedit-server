import {
  Entity,
  Enum,
  ManyToOne,
  PrimaryKey,
  PrimaryKeyType,
  SerializedPrimaryKey,
  Unique,
} from '@mikro-orm/core';
import { ObjectId } from '@mikro-orm/mongodb';
import { Field, ObjectType } from '@nestjs/graphql';
import { Post } from 'src/modules/post/entities/post.entity';
import { User } from 'src/modules/user/entities/user.entity';

export enum VoteType {
  UPVOTE = 1,
  DOWNVOTE = -1,
  UNVOTE = 0,
}

@ObjectType()
@Entity()
@Unique({
  name: 'postUserUnique',
  properties: ['user', 'post'],
})
export class Vote {
  @PrimaryKey()
  _id: ObjectId;

  @SerializedPrimaryKey()
  id: string;

  @Field(() => String)
  @Enum(() => VoteType)
  type!: VoteType;

  @Field(() => User)
  @ManyToOne(() => User)
  user!: User;

  @Field(() => Post)
  @ManyToOne(() => Post)
  post!: Post;

  constructor(type: VoteType, user: User, post: Post) {
    this.type = type;
    this.user = user;
    this.post = post;
  }
}
