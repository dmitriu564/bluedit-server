import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { Vote } from './entities/vote.entity';
import { VoteService } from './vote.service';

@Module({
  imports: [MikroOrmModule.forFeature([Vote])],
  providers: [VoteService],
  exports: [VoteService],
})
export class VoteModule {}
