import { EntityRepository, ObjectId } from '@mikro-orm/mongodb';
import { InjectRepository } from '@mikro-orm/nestjs';
import { Injectable } from '@nestjs/common';
import { Vote } from './entities/vote.entity';

export interface CombinedId {
  userId: string;
  postId: string;
}

@Injectable()
export class VoteService {
  constructor(
    @InjectRepository(Vote)
    private readonly voteRepository: EntityRepository<Vote>,
  ) {}

  //TODO: rewrite this garbage, possibly when switching to Prisma
  async findManyByCombinedIds(userIds: string[], postIds: string[]) {
    const votes = await this.voteRepository.find({
      user: {
        $in: userIds,
      },
    });
    const filteredVotes = votes.filter((v) => postIds.includes(v.post.id));
    return filteredVotes;
  }
}
