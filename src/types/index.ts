import { Request, Response } from 'express';
import { Session } from 'express-session';
import { Redis } from 'ioredis';
import { createPostDataloader } from 'src/utils/createPostDataloader';
import { createUserDataloader } from 'src/utils/createUserDataloader';
import { createVoteDataloader } from 'src/utils/createVoteDataloader';

declare module 'express-session' {
  interface Session {
    user: {
      [key: string]: any;
    };
    /*Could've been Omit<User, 'password'> 
        but mongodb uses _id and redis or express-session 
        hates underscore-first properties apparently*/
  }
}

export type SessionContext = {
  req: Request & { session: Session };
  res: Response;
  redis: Redis;
  userLoader: ReturnType<typeof createUserDataloader>;
  voteLoader: ReturnType<typeof createVoteDataloader>;
  postLoader: ReturnType<typeof createPostDataloader>;
};
