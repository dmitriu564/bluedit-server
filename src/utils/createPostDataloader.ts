import * as DataLoader from 'dataloader';
import { Post } from 'src/modules/post/entities/post.entity';
import { PostService } from 'src/modules/post/post.service';

export const createPostDataloader = (postService: PostService) =>
  new DataLoader<string, Post>(async (postIds: string[]) => {
    //No, that's not dumb, returned array must contain posts in exactly the order of the ids passed
    const posts = await postService.findManyByIds(postIds);

    const idToPost: Record<string, Post> = {};
    posts.forEach((post) => {
      idToPost[post.id] = post;
    });

    return postIds.map((id) => idToPost[id]);
  });
