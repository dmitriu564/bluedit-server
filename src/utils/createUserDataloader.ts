import * as DataLoader from 'dataloader';
import { User } from 'src/modules/user/entities/user.entity';
import { UserService } from 'src/modules/user/user.service';

export const createUserDataloader = (userService: UserService) =>
  new DataLoader<string, User>(async (userIds: string[]) => {
    //No, that's not dumb, returned array must contain users in exactly the order of the ids passed
    const users = await userService.findManyByIds(userIds);

    const idToUser: Record<string, User> = {};
    users.forEach((user) => {
      idToUser[user.id] = user;
    });

    return userIds.map((id) => idToUser[id]);
  });
