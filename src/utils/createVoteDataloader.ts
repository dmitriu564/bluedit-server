import * as DataLoader from 'dataloader';
import { Vote } from 'src/modules/vote/entities/vote.entity';
import { CombinedId, VoteService } from 'src/modules/vote/vote.service';

export const createVoteDataloader = (voteService: VoteService) =>
  new DataLoader<CombinedId, Vote>(async (combinedIds: CombinedId[]) => {
    //No, that's not dumb, returned array must contain entities in exactly the same order of the ids passed
    const { userIds, postIds } = combinedIds.reduce(
      (acc, cId) => {
        acc.userIds.push(cId.userId);
        acc.postIds.push(cId.postId);
        return acc;
      },
      { userIds: [] as string[], postIds: [] as string[] },
    );
    const votes = await voteService.findManyByCombinedIds(userIds, postIds);

    const idsToVote: Record<string, Vote> = {};
    votes.forEach((vote) => {
      idsToVote[`${vote.user.id}${vote.post.id}`] = vote;
    });

    return combinedIds.map((cId) => idsToVote[`${cId.userId}${cId.postId}`]);
  });
