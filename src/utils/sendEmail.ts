import * as dotenv from 'dotenv';
import * as nodemailer from 'nodemailer';

dotenv.config();

export async function sendEmail(to: string, subject: string, html: string) {
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.GMAIL_EMAIL,
      pass: process.env.GMAIL_PASS,
    },
  });

  const info = await transporter.sendMail({
    from: '"Bluedit Admin" <admin@bluedit.com>',
    to,
    subject,
    html,
  });

  console.log('Message sent: %s', info.messageId);
}
